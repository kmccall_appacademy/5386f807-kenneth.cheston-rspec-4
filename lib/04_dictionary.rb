class Dictionary


  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(entry)
    if entry.class == String
      @entries[entry] = nil
    else
      @entries = @entries.merge(entry)
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(key)
    @entries.include?(key)
  end

  def find(entry)
   @entries.select {|k| k.include?(entry)}
  end

  def printable
    string = ""
    @entries.sort.each { |k, v| string << %Q([#{k}] "#{v}"\n) }
    string.strip
  end

    #
    # @d = Dictionary.new
    # @d.add("zebra" => "African land animal with stripes")
    # @d.add("fish" => "aquatic animal")
    # @d.add("apple" => "fruit")
    # p @d.printable


# "{[apple] "fruit"\n[fish] "aquatic animal"\n[zebra] "African land animal with stripes""



end
