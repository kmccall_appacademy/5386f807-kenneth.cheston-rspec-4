class Temperature

  def initialize(option = {})
    @option = option
    @fahrenheit = @option[:f]
    @celsius = @option[:c]
  end

  def in_fahrenheit
      if @fahrenheit
        @fahrenheit
      else
        ctof(@celsius)
      end
  end

  def in_celsius
    if @fahrenheit
      ftoc(@fahrenheit)
    else
      @celsius
    end
  end

  def self.from_celsius(degrees)
    Temperature.new(c: degrees)
  end

  def self.from_fahrenheit(degrees)
    Temperature.new(f: degrees)
  end

  def ftoc(deg)
      (deg - 32) * (5.0 / 9.0)
    end

  def ctof(deg)
    deg * (9.0 / 5.0) + 32
  end

end


class Celsius < Temperature
  def initialize(deg)
    @celsius = deg
  end
end

class Fahrenheit < Temperature
  def initialize(deg)
    @fahrenheit = deg
  end
end
