
class MyHashSet

attr_accessor :store


  def initialize
    @store = {}
  end

  def insert(el)
    @store[el] = true
  end

  def include? (el)
    @store.key?(el)
  end

  def delete(el)

    if include?(el)
      @store.delete(el)
      return true
    else return false
    end
  end

  def to_a
    @store.keys.to_a
  end

  def union(set2)
     new_set = self.class.new
     self.to_a.each {|key,value| new_set.insert(key) if !new_set.include?(key)}
     set2.to_a.each {|key,value| new_set.insert(key) if !new_set.include?(key)}
     new_set
   end

  def intersect(set2)
      new_set = self.class.new
      self.to_a.each {|key| new_set.insert(key) if set2.include?(key)}
      new_set
    end

  def minus(set2)
      new_set = self.class.new
      self.to_a.each { |key| new_set.insert(key)}
      self.to_a.map { |key| new_set.delete(key) if set2.include?(key)}
      new_set
    end

  def symmetric_difference(set2)
    new_set = self.class.new
    self.to_a.each { |key| new_set.insert(key) unless set2.include?(key)}
    set2.to_a.each { |key| new_set.insert(key) unless self.include?(key)}
    puts new_set.to_a
  end

  def ==(object)
    return false unless object.class == MyHashSet && object.length == self.length

    self.each {|k,v| return false unless object.key?(k)}

    return true
  end
end
