
class Book

  attr_reader :title

  def title=(title)

    articles = ["the","a", "of", "an", "and", "in"]

    string = title.split(" ")
    string.each_with_index do |word, idx|
      word[0] = word[0].upcase unless articles.include?(word) && idx != 0
    end
    title = string.join(" ")

    @title = title
  end

end

@book = Book.new
p @book.title
