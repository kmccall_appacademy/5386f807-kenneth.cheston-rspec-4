
class Timer

  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def padded(num)
    return "0#{num}" if num < 10
    "#{num}"
  end

  def time_string
    hours = seconds / 3600
    minutes = (seconds % 3600) / 60
    timer_seconds = seconds % 60

    return "#{padded(hours)}:#{padded(minutes)}:#{padded(timer_seconds)}"
  end
end
